
    const btnCalcular = document.getElementById('btnCalcular');
    btnCalcular.addEventListener('click', function calcular()

    {
        let idPrecio = document.getElementById('idPrecio').value;
        let idPorcentaje = document.getElementById('idPorcentaje').value;
        let idPlazo = document.getElementById('idPlazo').value;


        let idPagoInicial = (idPrecio * (idPorcentaje/100));
        document.getElementById('idPagoInicial').value = idPagoInicial;


        let idTotalFin = (idPrecio - idPagoInicial);
        document.getElementById('idTotalFin').value = idTotalFin;

        let idPagoMensual = (idTotalFin / idPlazo);
        document.getElementById('idPagoMensual').value = idPagoMensual;


    });

    const btnLimpiar = document.getElementById('btnLimpiar');
    btnLimpiar.addEventListener('click', function limpiar()

    {
        document.getElementById('idPrecio').value = '';
        document.getElementById('idPorcentaje').value = '';
        document.getElementById('idPlazo').value = '';
        document.getElementById('idPagoInicial').value = '';
        document.getElementById('idTotalFin').value = '';
        document.getElementById('idPagoMensual').value = '';

    });

